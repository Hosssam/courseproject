package com.example.buyit;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 */
public class contact_us extends Fragment {
TextView email;
String name1;
String email1;
EditText msg;
Button send;

    public contact_us() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_contact_us, container, false);
        email=v.findViewById(R.id.contact_email);
        msg=v.findViewById(R.id.contact_msg);
        send=v.findViewById(R.id.contact_send);
        if(getArguments()!=null){
            getprofile(getArguments().getString("token"));
            send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(TextUtils.isEmpty(msg.getText().toString().trim())){
                        Toast.makeText(view.getContext(), "please write your comment", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        sendcomment(email1,name1,msg.getText().toString());
                        msg.setText("");
                    }
                }
            });
        }









        return v;
    }
    public void getprofile(String token){
        JSONObject object=new JSONObject();
        try {
            object.put("token",token);
            JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/get_profile.php", object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String status=response.getString("status");
                        if(Integer.parseInt(status)==0){
                            name1=response.getJSONObject("data").getString("name");
                            email1=response.getJSONObject("data").getString("email");
                            email.setText("Email : "+response.getJSONObject("data").getString("email"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(getContext());
            volleysingletoon.addrequest(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void sendcomment(String email, String name,  String msg){
       JSONObject object = new JSONObject();
        try {
            object.put("email",email);
            object.put("name",name);
            object.put("msg",msg);
            JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/contact_us.php", object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("sendcomment", "onResponse: "+response.toString());
                        String status=response.getString("status");
                        if(Integer.parseInt(status)==0){
                            Toast.makeText(getContext(), "sent", Toast.LENGTH_SHORT).show();


                        }
                        else{
                            Toast.makeText(getContext(), "failed", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(getContext());
            volleysingletoon.addrequest(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
