package com.example.buyit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.JsonToken;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class forgetpassword extends AppCompatActivity {
    String url1="http://training4mobile.net/mobile/forget_password.php";
    String url="http://training4mobile.net/mobile/check_pcode.php";
    String url2="http://training4mobile.net/mobile/add_new_password.php";
Button reset;
String token;
EditText forgetpassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpassword);
        reset=findViewById(R.id.reset);
        forgetpassword=findViewById(R.id.forgetpassword);
    }

    protected void onResume(){
        super.onResume();
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestreset();
            }
        });
    }






    public void requestreset(){
        JSONObject object=new JSONObject();
       if(!TextUtils.isEmpty(forgetpassword.getText())&&forgetpassword.getHint().equals("Your Email")){
           try {
               object.put("email",forgetpassword.getText());

           } catch (JSONException e) {
               e.printStackTrace();
           }
            JsonObjectRequest newrequest= new JsonObjectRequest(Request.Method.POST, url1, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("forget", "onResponse: "+response);
                       token=response.getString("token");
                        String status=response.getString("status");
                        Log.d("forget", "onResponse: "+status);
                        if(Integer.parseInt(status)==0){
                            Toast.makeText(forgetpassword.this, "check your email address for code", Toast.LENGTH_SHORT).show();
                            forgetpassword.setHint("Enter code");
                            forgetpassword.setText(null);
                            reset.setText("Submit");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("forget", "onErrorResponse: "+error);
                }
            });
           Volley.newRequestQueue(this).add(newrequest);

       }
       else if(!TextUtils.isEmpty(forgetpassword.getText())&&forgetpassword.getHint().equals("Enter code")){
           JSONObject resetobject= new JSONObject();
           try {
               resetobject.put("token",token);
               resetobject.put("code",forgetpassword.getText());
           } catch (JSONException e) {
               e.printStackTrace();
           }
       JsonObjectRequest myrequest=new JsonObjectRequest(Request.Method.POST, url, resetobject, new Response.Listener<JSONObject>() {
           @Override
           public void onResponse(JSONObject response) {
               try {
                   Log.d("code",response.toString());
                   String status=response.getString("status");
                   if(Integer.parseInt(status)==0){
                       Toast.makeText(forgetpassword.this, "Correct code", Toast.LENGTH_SHORT).show();
                       forgetpassword.setHint("Enter new password");
                       forgetpassword.setText("");
                   }
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
       }, new Response.ErrorListener() {
           @Override
           public void onErrorResponse(VolleyError error) {
               Log.d("forget", "onErrorResponse: "+error);
           }
       });
       Volley.newRequestQueue(this).add(myrequest);
       }
       else if(!TextUtils.isEmpty(forgetpassword.getText())&&forgetpassword.getHint().equals("Enter new password")){
           JSONObject password = new JSONObject();
           try {
               password.put("token",token);
               password.put("new_password",forgetpassword.getText().toString());
           } catch (JSONException e) {
               e.printStackTrace();
           }
           JsonObjectRequest objectRequest= new JsonObjectRequest(Request.Method.POST, url2, password, new Response.Listener<JSONObject>() {
               @Override
               public void onResponse(JSONObject response) {
                   try {
                       String status=response.getString("status");
                       if(Integer.parseInt(status)==0){
                           Toast.makeText(forgetpassword.this, "reset sucesfully", Toast.LENGTH_SHORT).show();
                           forgetpassword.setText("");
                           Intent y=new Intent(com.example.buyit.forgetpassword.this,MainActivity.class);
                           startActivity(y);
                       }
                       else{
                           Toast.makeText(forgetpassword.this, "failed"+response, Toast.LENGTH_SHORT).show();
                       }
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
               }
           }, new Response.ErrorListener() {
               @Override
               public void onErrorResponse(VolleyError error) {

               }
           });
           Volley.newRequestQueue(this).add(objectRequest);
       }
    }
}
