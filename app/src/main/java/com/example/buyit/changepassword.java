package com.example.buyit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class changepassword extends AppCompatActivity {
   String token;
   EditText oldpassword;
   EditText newpassword;
   Button change;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        token=getSharedPreferences("file",0).getString("token","null");
        oldpassword=findViewById(R.id.old_password);
        newpassword=findViewById(R.id.new_password);
        change=findViewById(R.id.change);
    }
    public void onResume(){
        super.onResume();
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(oldpassword.getText().toString().trim())&&!TextUtils.isEmpty(newpassword.getText().toString().trim())){
                    JSONObject object= new JSONObject();
                    try {
                        object.put("token",token);
                        object.put("old_password",oldpassword.getText().toString().trim());
                        object.put("new_password",newpassword.getText().toString().trim());
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/change_password.php", object, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    String status= response.getString("status");
                                    if(Integer.parseInt(status)==0){
                                        Toast.makeText(changepassword.this, "changed sucesfully", Toast.LENGTH_SHORT).show();

                                        SharedPreferences.Editor edit= getSharedPreferences("file",0).edit();
                                        edit.putString("password",newpassword.getText().toString());
                                        edit.commit();
                                        oldpassword.setText("");
                                        newpassword.setText("");
                                    }
                                    else if(Integer.parseInt(status)==225){
                                        Toast.makeText(changepassword.this, "wrong old password", Toast.LENGTH_SHORT).show();
                                    }
                                    else{
                                        Toast.makeText(changepassword.this, "failed", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });
                        volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(changepassword.this);
                        volleysingletoon.addrequest(request);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                else{
                    Toast.makeText(changepassword.this, "fill the information", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
