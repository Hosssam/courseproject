package com.example.buyit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class splash_screen extends AppCompatActivity {
    SharedPreferences myshared;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                myshared=getSharedPreferences("file",MODE_PRIVATE);
                String email=myshared.getString("email","null");
                String password=myshared.getString("password","null");
                if(!email.equals("null")&&!password.equals("null")){
                    loginbyshared(email,password);
                }
                else{
                    Intent z= new Intent(splash_screen.this,MainActivity.class);
                    startActivity(z);
                    finish();
                }
            }
        },4000);
    }
    public void loginbyshared(String email,String password){
        JSONObject object= new JSONObject();
        try {
            object.put("email",email);
            object.put("password",password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest myrequest=new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/login.php", object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String status=response.getString("status");
                    if(Integer.parseInt(status)==0){
                        SharedPreferences.Editor editor= myshared.edit();
                        editor.putString("token",response.getJSONObject("data").getString("token"));
                        editor.commit();
                        Intent y = new Intent(splash_screen.this,Home.class);
                        JSONObject user;
                        user=response.getJSONObject("data");


                        user senduser=new user(user.getString("token"),user.getString("name"),user.getString("email"),user.getString("img"));
                        y.putExtra("user",senduser);
                        Log.d("intent", "intentdata: "+senduser.getToken());
                        startActivity(y);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("editor error", "onErrorResponse: "+error);
            }
        });
        Volley.newRequestQueue(this).add(myrequest);
    }
}
