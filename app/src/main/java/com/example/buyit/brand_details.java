package com.example.buyit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class brand_details extends AppCompatActivity {
TextView name;
ImageView pic;
EditText searchbar;

brand brand;
ArrayList<product>products;
RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_details);

        int id =getIntent().getIntExtra("brand_position",0);
        brand = new brand();
      brand=brandsadapter.brands.get(id);
        name=findViewById(R.id.brand_nam1);
        pic=findViewById(R.id.logo);


        name.setText(brand.getName());
        Picasso.get().load(brand.getLogo()).into(pic);
    }
    public void onStart(){
        super.onStart();
        requestproducts();


    }
    public void requestproducts(){
        JSONObject object=new JSONObject();
        products=new ArrayList<>();
        try {
            object.put("token",getSharedPreferences("file",0).getString("token","0"));
            object.put("brand_id",brand.getId());
            JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST, Url.getbranddetail(), object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONArray array=response.getJSONObject("data").getJSONArray("products");
                        for(int i=0;i<array.length();i++){

                            Log.d("response_result", "onResponse: "+array.getJSONObject(i).toString());
                           String id=array.getJSONObject(i).getString("id");
                            String name=array.getJSONObject(i).getString("name");
                            double price=array.getJSONObject(i).getDouble("price");
                            String is_favourite=array.getJSONObject(i).getString("is_favourite");
                            String rate=array.getJSONObject(i).getString("rate");
                            String logo=array.getJSONObject(i).getString("logo");
                            String in_cart=array.getJSONObject(i).getString("in_cart");
                            product product=new product(id,name,price,is_favourite,rate,logo,in_cart);
                            products.add(product);
                            Log.d("results", "onResponse: "+price+"\n"+is_favourite+"\n"+rate+"\n"+logo+"\n"+in_cart+"\n"+products.size());
                        }
                        recyclerView=findViewById(R.id.products_rec);
                        recyclerView.setLayoutManager(new LinearLayoutManager(brand_details.this.getBaseContext()));
                        prdocutadapter adapter=new prdocutadapter(products,getSharedPreferences("file",0).getString("token","0"),1);
                        recyclerView.setAdapter(adapter);
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("error", "onErrorResponse: "+error.toString());

                }
            });


            volleysingletoon volly=volleysingletoon.getInstance(this);

            volly.addrequest(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
