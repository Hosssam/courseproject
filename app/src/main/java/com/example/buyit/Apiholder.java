package com.example.buyit;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface Apiholder {

    @Multipart
    @POST("edit_profile.php")
   // @FormUrlEncoded
    Call<Response> uploadImage(@Part("img") RequestBody image, @Part("token") String token,@Part("name") String name,@Part("email") String email,@Part("phone") String phone,@Part("address") String address,@Part("date_of_birth") String date,@Part("gender") String gender);
}
