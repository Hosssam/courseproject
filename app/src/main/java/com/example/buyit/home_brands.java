package com.example.buyit;


import android.content.Intent;
import android.media.Image;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class home_brands extends Fragment {
  static ArrayList<brand>brands;
  static RecyclerView recyclerView;
  static   brandsadapter myadapter;
  ImageView cart;
  String token;

    public home_brands() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_home_brands, container, false);

        recyclerView=v.findViewById(R.id.brandsrecycler);
        token=getArguments().getString("token");
        getBrands(getArguments().getString("token"));
        Home.search.setEnabled(true);
        cart=v.findViewById(R.id.send_order);
        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cartfrag cartfrag=new cartfrag();
                Bundle b=new Bundle();

                b.putString("token",token);
                cartfrag.setArguments(b);
               getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container,cartfrag).commit();
                Home.searchtext.setVisibility(View.GONE);
                Home.search.setEnabled(false);
               Home.navigationView.setCheckedItem(R.id.nav_cart);
            }
        });

        //brand x= new brand("2","Samsung","http:\\/\\/training4mobile.net\\/files\\/5d4238478c5860.56080388.png","0","35)");
        //brands.add(x);

        return v;
    }


    public void getBrands(String token){
        JSONObject askbrands=new JSONObject();
        try {
            askbrands.put("token",token);
            askbrands.put("page_number","1");
            askbrands.put("items_per_page","3");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Url url= new Url();
        JsonObjectRequest brandsrequest=new JsonObjectRequest(Request.Method.POST, url.getbrands(), askbrands, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("brands", "onResponse: "+response);
                try {
                    brands = new ArrayList<>();
                    JSONArray array=response.getJSONArray("data");
                    for(int i=0;i<array.length();i++){
                        String id=array.getJSONObject(i).getString("id");
                        String name=array.getJSONObject(i).getString("name");
                        String logo=array.getJSONObject(i).getString("logo");
                        String rate=array.getJSONObject(i).getString("rate");
                        String num_views=array.getJSONObject(i).getString("num_views");

                        brand brand = new brand(id,name,logo,rate,num_views);
                      brands.add(brand);

//                        Log.d("adaptersize", "onResponse: "+myadapter.brands.size());
//                        Log.d("getsize2", "onResponse: "+brands.size());
//                        Log.d("result", "onResulte: "+i+"  "+brand.toString());
                    }

                    myadapter=new brandsadapter(brands);
                    Log.d("getsize", "onCreateView: "+brands.size());
                    recyclerView.setAdapter(myadapter);

                    recyclerView.setLayoutManager(new LinearLayoutManager(home_brands.this.getContext()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        Volley.newRequestQueue(getContext()).add(brandsrequest);
    }
    public void onResume(){
        super.onResume();
        Home.search.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Home.searchtext.setVisibility(View.VISIBLE);
                Home.toolbar.setTitle("");
            }
        });
        Home.searchtext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                filterbrands(editable.toString());
            }
        });

    }
    public void filterbrands(String text){
        JSONObject object = new JSONObject();
        try {
            object.put("token",getArguments().getString("token"));
            object.put("search_string",text);
            object.put("page_number","1");
            object.put("items_per_page","100");
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/filter_brands.php", object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("filter", "onResponse: "+response.getJSONArray("data"));
                        brands = new ArrayList<>();
                        JSONArray array=response.getJSONArray("data");
                        for(int i=0;i<array.length();i++) {
                            String id = array.getJSONObject(i).getString("id");
                            String name = array.getJSONObject(i).getString("name");
                            String logo = array.getJSONObject(i).getString("logo");
                            String rate = array.getJSONObject(i).getString("rate");
                            String num_views = array.getJSONObject(i).getString("num_views");

                            brand brand = new brand(id, name, logo, rate, num_views);
                            brands.add(brand);
                        }
                        myadapter=new brandsadapter(brands);

                        recyclerView.setAdapter(myadapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(getContext());
            volleysingletoon.addrequest(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
