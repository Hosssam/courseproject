package com.example.buyit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;

public class SliderAdapterExample extends SliderViewAdapter<SliderAdapterExample.SliderAdapterVH> {


    private Context context;
    private  String url1;
    private  String url;
    public SliderAdapterExample(Context context,String url,String url1) {
        this.context = context;
        this.url=url;
        this.url1=url1;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_row, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {


        switch (position) {
            case 0:
                Picasso.get()
                        .load(url)
                        .into(viewHolder.imageViewBackground);
                break;
            case 1:
                Picasso.get()
                        .load(url1)
                        .into(viewHolder.imageViewBackground);
                break;


            default:

                break;

        }

    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return 2;
    }

 public static class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;


        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.image_row1);

            this.itemView = itemView;
        }
    }
}
