package com.example.buyit;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ProfileActivity extends AppCompatActivity {
    ImageView profile;
    private Uri image;
    private  int image_request=1;
    user myuser;
    String profiletoken;
    Bitmap bitmap;
   EditText name;
   EditText pass;
    File imageFile;
   EditText address;
   Button edit;
   EditText email;
   EditText phone;
   TextView date;
   TextView gender;
   Url url = new Url();
   String TAG="ProfileActivity";
   String status="view";
    DatePickerDialog.OnDateSetListener listener;
    Button save;
    Button upload,changepass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        upload=findViewById(R.id.upload);
       upload.setVisibility(View.GONE);
        profiletoken=getIntent().getStringExtra("token");
        name=findViewById(R.id.nav_name);
        address=findViewById(R.id.address1);
        email=findViewById(R.id.email);
        changepass=findViewById(R.id.change_pass);
        phone=findViewById(R.id.phone);
        date=findViewById(R.id.date);
        gender=findViewById(R.id.gender);
        edit=findViewById(R.id.edit);
        save=findViewById(R.id.save);
        profile=findViewById(R.id.profile_img);
        listener= new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month=month+1;
                String datee=day+"/"+month+"/"+year;
          date.setText(datee);
            }
        };
        connectvolley();

      save.setVisibility(View.GONE);

        Log.d(TAG, "onCreate: "+profiletoken);

    }
   public void onResume(){
        super.onResume();


       date.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               Calendar cal= Calendar.getInstance();
               int year=cal.get(Calendar.YEAR);
               int month=cal.get(Calendar.MONTH);
               int day=cal.get(Calendar.DAY_OF_MONTH);
               DatePickerDialog dialog= new DatePickerDialog(
                       ProfileActivity.this,
                       android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                       listener,year,month,day
               );
               dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
               dialog.show();
           }
       });
     changepass.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             Intent z= new Intent(ProfileActivity.this,changepassword.class);
             startActivity(z);
         }
     });
   }










    public void connectvolley(){
        JSONObject object= new JSONObject();
        try {
            object.put("token",profiletoken);
            Log.d(TAG, "connectvolley: "+profiletoken);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        JsonObjectRequest myrequest=new JsonObjectRequest(Request.Method.POST, url.getprofile(), object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
               // Log.d(TAG, "onResponse: "+response.toString());

                try {

                        String name = response.getJSONObject("data").getString("name");
                        String email = response.getJSONObject("data").getString("email");
                        String phone = response.getJSONObject("data").getString("phone");
                        String address = response.getJSONObject("data").getString("address");
                        String date = response.getJSONObject("data").getString("date_of_birth");
                        String gender = response.getJSONObject("data").getString("gender");
                        String img = response.getJSONObject("data").getString("img");

                        myuser = new user(profiletoken,name,email,img);
                        myuser.setAddress(address);
                        myuser.setGender(gender);
                        myuser.setDateofbirth(date);
                        myuser.setPhone(phone);
                    Picasso.get().load(img).into(profile);
                        seteditext(myuser);
                        Log.d(TAG, "onResponse: "+name+email+phone+address+date+gender+img);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: "+error.getMessage()+error.toString());
            }
        });
      Volley.newRequestQueue(this).add(myrequest);

    }















    public void seteditext(user user){

        name.setEnabled(false);

        address.setEnabled(false);

        email.setEnabled(false);

        phone.setEnabled(false);

        date.setEnabled(false);

        if(user!=null){
            Log.d(TAG, "seteditext: "+user.getName());
            name.setText(user.getName());
            address.setText(user.getAddress());
            email.setText(user.getEmail());
            phone.setText(user.getPhone());
            date.setText(user.getDateofbirth());
            gender.setText(user.getGender());
            edit.setEnabled(true);
        }

    }
    public void edit(View v){

    if(edit.getText().equals("Edit")){

       name.setHint( "Enter new name");
      address.setHint(  "Enter new address");
        email.setHint("Enter new email");
       phone.setHint( "Enter new phone number");
      date.setHint(  "Enteer new date");
       gender.setHint( "Enter your gender");
        name.setText("");
        address.setText("");
        email.setText("");
        phone.setText("");
        date.setText("");
        gender.setText("");
        name.setEnabled(true);

        address.setEnabled(true);

        email.setEnabled(true);

        phone.setEnabled(true);

        date.setEnabled(true);
        status="waiting for information";
        save.setVisibility(View.VISIBLE);
        upload.setVisibility(View.VISIBLE);
        edit.setEnabled(false);


    }

        Log.d(TAG, "edit: wslt el7 bslama");




    }
    public void save(View v){
        RadioGroup radioGroup=findViewById(R.id.radioGroup);
        int id=1;
         id=radioGroup.getCheckedRadioButtonId();

        RadioButton x=findViewById(id);


        if(!TextUtils.isEmpty(name.getText().toString().trim())&&!TextUtils.isEmpty(email.getText().toString().trim())&&!TextUtils.isEmpty(phone.getText().toString().trim())&&!TextUtils.isEmpty(address.getText().toString().trim())&&!TextUtils.isEmpty(date.getText().toString().trim())&&id!=1&&image!=null){
            Log.d(TAG, "save: el7 wslna");
            //updateinfo();
            Retrofit();
        }
        else{
            Toast.makeText(this, "fill all the information pls", Toast.LENGTH_SHORT).show();
        }
    }
    public void updateinfo(){
    JSONObject object=new JSONObject();

        try {
            object.put("token",getSharedPreferences("file",MODE_PRIVATE).getString("token","null"));
            object.put("name",name.getText().toString().trim());
            object.put("email",email.getText().toString().trim());
            object.put("phone",phone.getText().toString().trim());
            object.put("address",address.getText().toString().trim());
            object.put("date_of_birth",date.getText().toString().trim());

            Log.d(TAG, "a3ml upload b2ah dnta bta3 mostfz"+bitmap);

            object.put("img",imagetostring(bitmap));

            RadioGroup radioGroup=findViewById(R.id.radioGroup);
            int id=radioGroup.getCheckedRadioButtonId();

            RadioButton x=findViewById(id);
            object.put("gender",x.getText());
       JsonObjectRequest request =new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/edit_profile.php", object, new Response.Listener<JSONObject>() {
             @Override
             public void onResponse(JSONObject response) {
                 try {
                     String status=response.getString("status");
                     if(Integer.parseInt(status)==0){

                     Log.d(TAG, "lolololy: "+response.toString());
                     save.setVisibility(View.GONE);
                     upload.setVisibility(View.GONE);
                     edit.setEnabled(true);
                     connectvolley();
                     }
                 } catch (JSONException e) {
                     e.printStackTrace();
                 }

             }
         }, new Response.ErrorListener() {
             @Override
             public void onErrorResponse(VolleyError error) {
                 Log.d(TAG, "onErrorResponse: "+error);
             }
         });
            Volley.newRequestQueue(this).add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    public void upload(View v){
        Intent z=new Intent(Intent.ACTION_GET_CONTENT);
        z.setType("image/*");
        startActivityForResult(Intent.createChooser(z,"select an image"),image_request);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==image_request&&resultCode==RESULT_OK&&data!=null&&data.getData()!=null){
            image=data.getData();

           String path=getPath(getApplicationContext(),image);
            Log.d(TAG, "onActivityResult:path "+path);
            // bitmap= MediaStore.Images.Media.getBitmap(getContentResolver(),Uri.parse(path));


             imageFile = new File(path);
            Log.d(TAG, "onActivityResult: "+imageFile.toString());
          BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
           bitmap = BitmapFactory.decodeFile(imageFile.toString(), options);


            Log.d(TAG, "onActivityResult abs: "+imageFile.getAbsolutePath()+"\n"+imageFile.getPath());
            bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            Log.d(TAG, "onActivityResult bitmap: "+bitmap);
     profile.setImageBitmap(bitmap);
            Log.d(TAG, "onActivityResult:data "+data);
            Log.d(TAG, "onActivityResult: data.getdata"+data.getData());
            
        }
    }


    private String imagetostring(Bitmap bitmap){
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imgebytes=byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(imgebytes,Base64.DEFAULT);
    }
    public static String getPath( Context context, Uri uri ) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent z= new Intent(ProfileActivity.this,Home.class);

        startActivity(z);
    }



    public void Retrofit(){
        Retrofit retrofit=new Retrofit.Builder().baseUrl("http://training4mobile.net/mobile/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        Apiholder holder= retrofit.create(Apiholder.class);
        Map<String,String> hash=new HashMap<>();
        hash.put("token",getSharedPreferences("file",MODE_PRIVATE).getString("token","null"));
       hash. put("name",name.getText().toString().trim());
        hash.put("email",email.getText().toString().trim());
        hash.put("phone",phone.getText().toString().trim());
        hash.put("address",address.getText().toString().trim());
        hash.put("date_of_birth",date.getText().toString().trim());
      //  Log.d(TAG, "Retrofit: token"+getSharedPreferences("file",MODE_PRIVATE).getString("token","null"));
        RadioGroup radioGroup=findViewById(R.id.radioGroup);
        int id=radioGroup.getCheckedRadioButtonId();
        RadioButton x=findViewById(id);
        hash.put("gender",x.getText().toString());

        RequestBody requestBody=RequestBody.create(MediaType.parse("image/*"), imageFile);
      /*  MultipartBody.Part body =
                MultipartBody.Part.createFormData("img", "img", requestBody); */

        Log.d(TAG, "Retrofit: "+requestBody.toString());
        Log.d(TAG, "Retrofit: which sent"+getSharedPreferences("file",MODE_PRIVATE).getString("token","null")+"\n"+name.getText().toString().trim()+"\n"+email.getText().toString().trim()+"\n"+phone.getText().toString().trim()+"\n"+address.getText().toString().trim()+"\n"+date.getText().toString()+"\n"+x.getText().toString());
        Call<com.example.buyit.Response> call=holder.uploadImage(requestBody,getSharedPreferences("file",MODE_PRIVATE).getString("token","null"),name.getText().toString().trim(),email.getText().toString().trim(),phone.getText().toString().trim(),address.getText().toString().trim(),date.getText().toString().trim(),x.getText().toString());
        call.enqueue(new Callback<com.example.buyit.Response>() {
            @Override
            public void onResponse(Call<com.example.buyit.Response> call, retrofit2.Response<com.example.buyit.Response> response) {
                Log.d(TAG, "Retrofit wi rbna ystor : "+response.body().getStatus());
                Log.d(TAG, "Retrofit wi rbna ystor : "+response.body().getMsg());


            }

            @Override
            public void onFailure(Call<com.example.buyit.Response> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());

            }
        });
    }
}
