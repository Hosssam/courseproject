package com.example.buyit;

public class cart {
  String id;
   String name;
    double price;
     String logo;
     String rate;
      String frequency;

    public cart(String id, String name, double price, String logo, String frequency,String rate) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.logo = logo;
        this.frequency = frequency;
        this.rate=rate;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }
}
