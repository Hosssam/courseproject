package com.example.buyit;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class cartfrag extends Fragment {
ArrayList<cart>carts;
RecyclerView recyclerView;
TextView txt;
ImageView favourtie;
    public cartfrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_cartfrag, container, false);
        txt=v.findViewById(R.id.send_order);
        favourtie=v.findViewById(R.id.favourite);

        if(getArguments()!=null){
            requestcarts(getArguments().getString("token"));
            Log.d("token", "onCreateView: "+getArguments().getString("token"));
           txt.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   order(getArguments().getString("token"));
               }
           });
            favourtie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   favourite_frag cartfrag=new favourite_frag();
                    Bundle b=new Bundle();

                    b.putString("token",getArguments().getString("token"));
                    cartfrag.setArguments(b);

                    getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container,cartfrag).commit();
                   Home.navigationView.setCheckedItem(R.id.nav_favourties);

                }
            });
        }
       recyclerView=v.findViewById(R.id.cart_rec);
        return v;
    }
    public void requestcarts(String token){
        JSONObject object= new JSONObject();
        carts=new ArrayList<>();
        try {
            object.put("token",token);
            JsonObjectRequest request= new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/get_cart.php", object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    JSONArray array= new JSONArray();
                    try {
                        array=response.getJSONArray("data");
                        for(int i=0;i<array.length();i++){
                            String id=array.getJSONObject(i).getString("id");
                            String name=array.getJSONObject(i).getString("name");
                            double price=array.getJSONObject(i).getDouble("price");
                            String rate=array.getJSONObject(i).getString("rate");
                            String logo=array.getJSONObject(i).getString("logo");
                            String frequency=array.getJSONObject(i).getString("frequency");
                            cart cart= new cart(id,name,price,logo,frequency,rate);
                            carts.add(cart);
                        }
                    cartadapter myadapter=new cartadapter(carts);
                    recyclerView.setAdapter(myadapter);

                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                   myadapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            volleysingletoon volly=volleysingletoon.getInstance(getContext());

            volly.addrequest(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    public void order(final String token){
        JSONObject object= new JSONObject();
        try {
            object.put("token",token);
            final JsonObjectRequest request= new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/send_order.php", object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String status=response.getString("status");
                        if(Integer.parseInt(status)==0){
                            Toast.makeText(getContext(), "done", Toast.LENGTH_SHORT).show();
                            requestcarts(token);
                        }
                        else if(Integer.parseInt(status)==224){
                            Toast.makeText(getContext(), "cart is empty", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(getContext(), "error", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(getContext());
        volleysingletoon.addrequest(request);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
