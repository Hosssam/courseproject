package com.example.buyit;

import android.content.Context;

import androidx.core.util.Pools;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class volleysingletoon {
    private static volleysingletoon Instance;
    private static Context mycontext;
    private RequestQueue queue;

    private volleysingletoon(Context cx){
        this.mycontext=cx;
    }
    public  static synchronized   volleysingletoon getInstance(Context cx){
        if(Instance==null){
            Instance=new volleysingletoon(cx);

        }
        return Instance;
    }
    public RequestQueue getrequestqueue(){
        if(queue==null){
            queue= Volley.newRequestQueue(mycontext.getApplicationContext());
        }
       return queue;
    }
    public<T> void addrequest(Request request){
      getrequestqueue().add(request);
    }
}
