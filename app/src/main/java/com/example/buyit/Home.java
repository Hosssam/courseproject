package com.example.buyit;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import android.util.Log;
import android.view.View;

import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
static Toolbar toolbar;
    user myuser;
    String token;
    TextView name;
    ImageView profile;
  static  ImageView search;
   static EditText searchtext;
   ImageView cart;
     static NavigationView navigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        token=getSharedPreferences("file",MODE_PRIVATE).getString("token","null");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

      cart=findViewById(R.id.disabled_cart);
      cart.setVisibility(View.GONE);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
      navigationView = findViewById(R.id.nav_view);
        View header=navigationView.getHeaderView(0);
        name=header.findViewById(R.id.nav_name);
        profile=header.findViewById(R.id.nav_profileimg);
        search=findViewById(R.id.searchbutton);
        searchtext=findViewById(R.id.searchbar);
        getprofile();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        home_brands fragment=new home_brands();
        Bundle b=new Bundle();
        String mytoken=getSharedPreferences("file",MODE_PRIVATE).getString("token","null");
        Log.d("tokenzft", "onNavigationItemSelected: "+mytoken);
        b.putString("token",token);
        fragment.setArguments(b);
        getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();
        Home.navigationView.setCheckedItem(R.id.nav_home);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            setToolbar();

            home_brands fragment=new home_brands();
            Bundle b=new Bundle();
            String mytoken=getSharedPreferences("file",MODE_PRIVATE).getString("token","null");
            Log.d("tokenzft", "onNavigationItemSelected: "+mytoken);
            b.putString("token",token);
            fragment.setArguments(b);
            getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();

        }
        else if (id == R.id.nav_cart) {
            Home.searchtext.setVisibility(View.GONE);
            Home.search.setEnabled(false);
         cartfrag cartfrag=new cartfrag();
            Bundle b=new Bundle();
            toolbar.setTitle("");
            b.putString("token",token);
            cartfrag.setArguments(b);
            getSupportFragmentManager().beginTransaction().replace(R.id.container,cartfrag).commit();
        } else if (id == R.id.nav_favourties) {
            toolbar.setTitle("");
            Home.search.setEnabled(false);
            Home.searchtext.setVisibility(View.GONE);
            favourite_frag cartfrag=new favourite_frag();
            Bundle b=new Bundle();

            b.putString("token",token);
            cartfrag.setArguments(b);
            getSupportFragmentManager().beginTransaction().replace(R.id.container,cartfrag).commit();


        } else if (id == R.id.nav_contact) {
            toolbar.setTitle("");
            Home.search.setEnabled(false);
            Home.searchtext.setVisibility(View.GONE);
         contact_us frag= new contact_us();
            Bundle b=new Bundle();

            b.putString("token",token);
            frag.setArguments(b);
         toolbar.setTitle("");
         getSupportFragmentManager().beginTransaction().replace(R.id.container,frag).commit();
         navigationView.setCheckedItem(R.id.nav_contact);

        } else if (id == R.id.nav_profileimg) {

            Home.search.setEnabled(false);
            Home.searchtext.setVisibility(View.GONE);
      Intent z = new Intent(this,ProfileActivity.class);
      z.putExtra("token",token);
      startActivity(z);


        }
        else if (id == R.id.nav_logout) {
            Home.search.setEnabled(false);
            Home.searchtext.setVisibility(View.GONE);
            SharedPreferences.Editor edit=getSharedPreferences("file",0).edit();
            edit.putString("email","null");
            edit.putString("password","null");
            edit.commit();
            Intent y = new Intent(this,MainActivity.class);
            startActivity(y);
            finish();

        }

        else if (id == R.id.nav_about) {
            Home.search.setEnabled(false);
            Home.searchtext.setVisibility(View.GONE);
            aboutus();
       Intent z = new Intent(Intent.ACTION_VIEW);

       z.setData(Uri.parse("http://training4mobile.net/mobile/get_about_us.php"));
       startActivity(z);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    protected void onResume()
    {
        super.onResume();

       myuser= (user)getIntent().getSerializableExtra("user");

//        Log.d("user", "onResume: "+myuser.getName());
    }
    public void setToolbar(){
        toolbar.setTitle("");
        ImageView cart=findViewById(R.id.disabled_cart);
        cart.setVisibility(View.GONE);
    }
    public void aboutus(){
        JsonObjectRequest request= new JsonObjectRequest(Request.Method.GET, "http://training4mobile.net/mobile/get_about_us.php", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("about us", "onResponse: "+response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", "onErrorResponse: "+error.toString());
            }
        });
        volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(this);
        volleysingletoon.addrequest(request);
    }
    public void getprofile(){
        JSONObject object=new JSONObject();
        try {
            object.put("token",token);
            JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/get_profile.php", object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        String status=response.getString("status");
                        if(Integer.parseInt(status)==0){
                            name.setText(response.getJSONObject("data").getString("name"));
                           // Picasso.get().load(response.getJSONObject("data").getString("img")).into(profile);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(this);
            volleysingletoon.addrequest(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
