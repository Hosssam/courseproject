package com.example.buyit;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class brandsadapter extends RecyclerView.Adapter<brandsadapter.holder> {
static ArrayList<brand>brands = new ArrayList<>();

    public brandsadapter(ArrayList<brand> brands) {
        this.brands = brands;
    }

    @NonNull
    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.brandrow,parent,false);
        holder x = new holder(v);
        return x;

    }

    @Override
    public void onBindViewHolder(@NonNull holder holder, final int position) {
    holder.name.setText(brands.get(position).getName());
        Picasso.get().load(brands.get(position).getLogo()).into(holder.logo);
        String rate=brands.get(position).getRate();
        if(Float.parseFloat(rate)==0){
            holder.star1.setImageResource(R.drawable.ic_star4);
            holder.star2.setImageResource(R.drawable.ic_star4);
            holder.star3.setImageResource(R.drawable.ic_star4);
            holder.star4.setImageResource(R.drawable.ic_star4);
            holder.star5.setImageResource(R.drawable.ic_star4);
        }
        else if(Float.parseFloat(rate)==1){
            holder.star2.setImageResource(R.drawable.ic_star4);
            holder.star3.setImageResource(R.drawable.ic_star4);
            holder.star4.setImageResource(R.drawable.ic_star4);
            holder.star5.setImageResource(R.drawable.ic_star4);
        }
        else if(Float.parseFloat(rate)==2) {
            holder.star3.setImageResource(R.drawable.ic_star4);
            holder.star4.setImageResource(R.drawable.ic_star4);
            holder.star5.setImageResource(R.drawable.ic_star4);
        }
        else if(Float.parseFloat(rate)==3){
            holder.star4.setImageResource(R.drawable.ic_star4);
            holder.star5.setImageResource(R.drawable.ic_star4);
        }
        else if(Float.parseFloat(rate)==4){
            holder.star5.setImageResource(R.drawable.ic_star4);
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent z = new Intent(view.getContext(),brand_details.class);
               z.putExtra("brand_position",position);
               view.getContext().startActivity(z);
            }
        });
    }

    @Override
    public int getItemCount() {
        return brands.size();
    }

    public static class holder extends RecyclerView.ViewHolder{
        ImageView star1;
        ImageView star2;
        ImageView star3;
        ImageView star4;
        ImageView star5;
        ImageView logo;
        TextView name;
        ConstraintLayout layout;
        public holder(@NonNull View itemView) {
            super(itemView);
            star1=itemView.findViewById(R.id.star1);
            star2=itemView.findViewById(R.id.star2);
            star3=itemView.findViewById(R.id.star3);
            star4=itemView.findViewById(R.id.star4);
            star5=itemView.findViewById(R.id.star5);
            logo=itemView.findViewById(R.id.logo);
            name=itemView.findViewById(R.id.nav_name);
            layout=itemView.findViewById(R.id.brandlayout);
        }
    }
}
