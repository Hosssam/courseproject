package com.example.buyit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class Register extends AppCompatActivity {
EditText name;
EditText password;
EditText email;
EditText confirmpassword;
Button button;
String username;
String userpassword;
String email2;
String confirmpassword2;
String url="http://training4mobile.net/mobile/signup.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        name=findViewById(R.id.register_name);
        email=findViewById(R.id.register_email);
        password=findViewById(R.id.register_password);
        confirmpassword=findViewById(R.id.register_password1);
        button=findViewById(R.id.signup);
    }
    public void onResume() {
        super.onResume();

     button.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             if (getData()) {
                 if (!confirmpassword2.equals(userpassword)) {
                     Toast.makeText(Register.this, "Password and cofirm password dont match", Toast.LENGTH_SHORT).show();
                 } else if(confirmpassword2.equals(userpassword)) {
                  //   Toast.makeText(Register.this, "match", Toast.LENGTH_SHORT).show();
                     adduser();
                 }
             }
             else {
                 Toast.makeText(Register.this, "Please fill all the information", Toast.LENGTH_SHORT).show();
             }
         }
     });


    }




    public boolean getData(){

        if(!TextUtils.isEmpty(name.getText().toString().trim())&&!TextUtils.isEmpty(email.getText().toString().trim())&&!TextUtils.isEmpty(password.getText().toString().trim())&&!TextUtils.isEmpty(confirmpassword.getText().toString().trim())) {
          username=  name.getText().toString();
          userpassword= password.getText().toString();

          confirmpassword2=  confirmpassword.getText().toString();

           email2= email.getText().toString();
        return true;

        }
return false;
    }
    public void adduser(){
        JSONObject user =new JSONObject();
        try {
            user.put("name",username);
            user.put("email",email2);
            user.put("password",userpassword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.POST, url, user, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
           Log.d("response","Response"+response.toString());
                Toast.makeText(Register.this, "Sucesfully Created", Toast.LENGTH_SHORT).show();
                Intent z= new Intent(Register.this,MainActivity.class);
                startActivity(z);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", "onErrorResponse: "+error.toString());
            }
        });
    Volley.newRequestQueue(this).add(jsonObjectRequest);
    }




}
