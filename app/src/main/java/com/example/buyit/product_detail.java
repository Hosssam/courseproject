package com.example.buyit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class product_detail extends AppCompatActivity {
String product_id,token;
SliderAdapterExample sliderAdapterExample;
SliderView view;
TextView name;
TextView product;
TextView description;
TextView addtocart;
EditText comment;
RatingBar ratingBar;
Button sendcomment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        product_id=getIntent().getStringExtra("product_id");
        token=getSharedPreferences("file",0).getString("token","null");
        view=findViewById(R.id.slide);
        name=findViewById(R.id.product_name1);
        product=findViewById(R.id.product_price1);
         description=findViewById(R.id.product_description);
          addtocart=findViewById(R.id.addtocart);
          comment=findViewById(R.id.comment);
          ratingBar=findViewById(R.id.ratingBar);
          sendcomment=findViewById(R.id.sendcomment);
          addtocart.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(final View view) {

                  JSONObject object = new JSONObject();
                  try {
                      object.put("token", token);
                      object.put("product_id", product_id);
                      JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/add_cart.php", object, new Response.Listener<JSONObject>() {
                          @Override
                          public void onResponse(JSONObject response) {
                              try {
                                  Log.d("error", "onResponse: "+response.getString("status"));
                                  if (Integer.parseInt(response.getString("status")) == 0) {
                                      Toast.makeText(view.getContext(), "sucessfully added,check your cart", Toast.LENGTH_SHORT).show();
                                  } else if (Integer.parseInt(response.getString("status")) == 222) {

                                      Toast.makeText(view.getContext(), "already in cart", Toast.LENGTH_SHORT).show();
                                  } else {
                                      Toast.makeText(view.getContext(), "something went wrong", Toast.LENGTH_SHORT).show();
                                  }
                              } catch (JSONException e) {
                                  e.printStackTrace();
                              }
                          }
                      }, new Response.ErrorListener() {
                          @Override
                          public void onErrorResponse(VolleyError error) {

                          }
                      });
                      volleysingletoon volleysingletoon = com.example.buyit.volleysingletoon.getInstance(view.getContext());
                      volleysingletoon.addrequest(request);
                  } catch (JSONException e) {
                      e.printStackTrace();
                  }

              }
          });
        getproduct(token,product_id);


        sendcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(comment.getText().toString().trim())){
                    addrate(token,product_id);
                }
                else{
                    Toast.makeText(product_detail.this, "write comment", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }









    public void getproduct(String token,String product_id){
        JSONObject object=new JSONObject();
        try {
            object.put("token",token);
            object.put("product_id",product_id);
            JsonObjectRequest request= new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/product_details.php", object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("product_details", "onResponse: "+response.toString());
                    try {
                        JSONObject data=response.getJSONObject("data");
                        name.setText(data.getString("name"));
                        product.setText("Price :"+data.getString("price"));
                        description.setText(data.getString("descr"));

                        JSONArray url=data.getJSONArray("images");
                        String url1=url.getJSONObject(0).getString("url");
                        String url2=url.getJSONObject(1).getString("url");
                        Log.d("url", "onResponse: "+url1+"\n"+url2);
                        sliderAdapterExample=new SliderAdapterExample(product_detail.this,url1,url2);
                        view.setSliderAdapter(sliderAdapterExample);
                        view.setAutoCycle(false);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(this);
            volleysingletoon.addrequest(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
    public void addrate(String token,String product_id){
    JSONObject object= new JSONObject();
        try {
            object.put("token",token);
            object.put("product_id",product_id);
            object.put("rate",ratingBar.getRating());
            object.put("comment",comment.getText().toString().trim());
            JsonObjectRequest objectRequest= new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/add_rate.php", object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        Log.d("send comment", "onResponse: "+response.toString());
                        if(Integer.parseInt(response.getString("status"))==200){
                            Toast.makeText(product_detail.this, "send sucesfully", Toast.LENGTH_SHORT).show();
                            comment.setText("");
                        }
                        else if(Integer.parseInt(response.getString("status"))==505){
                            Toast.makeText(product_detail.this, "error", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(this);
            volleysingletoon.addrequest(objectRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
