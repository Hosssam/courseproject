package com.example.buyit;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {
    TextView register;
    String url="http://training4mobile.net/mobile/login.php";
    Button login;
    EditText email;
    EditText password;
    String username;
    String password1;
    TextView forget;
    String status;
    Switch on;
    SharedPreferences myshared;
    String token;
    user myuser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        email=findViewById(R.id.login_email);
        password=findViewById(R.id.login_password);
        register=findViewById(R.id.register);
        login=findViewById(R.id.login);
        forget=findViewById(R.id.forget);
        on=findViewById(R.id.switch1);
        myshared=getSharedPreferences("file",MODE_PRIVATE);
        String email=myshared.getString("email","null");
        String password=myshared.getString("password","null");


    }
    protected void onStart(){
        super.onStart();
        String text="Don't have an account?Register";
        ForegroundColorSpan foregroundColorSpan=new ForegroundColorSpan(getResources().getColor(R.color.logo));
        ClickableSpan clickableSpan= new ClickableSpan() {
            @Override
            public void onClick(View view) {
                Intent x = new Intent(MainActivity.this,Register.class);
                startActivity(x);
            }
        };
        SpannableString spannableString=new SpannableString(text);
        spannableString.setSpan(foregroundColorSpan,22,30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(clickableSpan,22,30, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        register.setText(spannableString);
        register.setMovementMethod(new LinkMovementMethod());

    }
    protected void onResume(){
        super.onResume();
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(getData()){
                volleyconnect();
                if(on.isChecked()){

                    SharedPreferences.Editor editor=myshared.edit();
                    String name=email.getText().toString();
                    Log.d("shared1", "onClick: "+name);
                    String pass=password.getText().toString();
                    editor.putString("email",name);
                    editor.putString("password",pass);

//                    Log.d("test", "onClick: "+myuser.getToken());
                    editor.commit();
                    Log.d("shared", "onClick: "+myshared.getString("email","null"));
                }
                else{
                    SharedPreferences.Editor editor=myshared.edit();
                    editor.putString("email","null");
                    editor.putString("password","null");
                    editor.commit();
                }

                }
                else{
                    Toast.makeText(MainActivity.this, "fill information", Toast.LENGTH_SHORT).show();
                }
            }
        });

forget.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Intent y= new Intent(MainActivity.this,forgetpassword.class);
        startActivity(y);
    }
});
    }
    public boolean getData(){
        if(!TextUtils.isEmpty(email.getText().toString().trim())&&!TextUtils.isEmpty(password.getText().toString().trim())){
            username=email.getText().toString();
            password1=password.getText().toString();
            return true;
        }
        return false;
    }
    public void volleyconnect(){
        JSONObject object = new JSONObject();

        try {
            object.put("email",username);
            object.put("password",password1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final JsonObjectRequest request= new JsonObjectRequest(Request.Method.POST, url, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("login", "onResponse: "+response);
                try {
                    status=response.getString("status");
                    if(Integer.parseInt(status)==0){
                        Intent y = new Intent(MainActivity.this,Home.class);
                        JSONObject user;
                        user=response.getJSONObject("data");
                        myuser=new user(user.getString("token"),user.getString("name"),user.getString("email"),user.getString("img"));
                        SharedPreferences.Editor editor= myshared.edit();
                        editor.putString("token",response.getJSONObject("data").getString("token"));
                        editor.commit();
                        Log.d("intent", "intentdata: "+myuser.toString());
                        y.putExtra("user",myuser);
                        startActivity(y);
                        finish();
                    }
                    else{
                        Toast.makeText(MainActivity.this, "Wrong password or Email", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("login", "onErrorResponse: "+error);
            }
        });
        Volley.newRequestQueue(this).add(request);
    }

}
