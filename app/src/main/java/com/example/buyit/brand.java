package com.example.buyit;

public class brand {
  private   String id;
  private   String name;
  private   String logo;
  private   String rate;
  private   String num_views;

    public brand() {
    }

    public brand(String id, String name, String logo, String rate, String num_views) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.rate = rate;
        this.num_views = num_views;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getNum_views() {
        return num_views;
    }

    public void setNum_views(String num_views) {
        this.num_views = num_views;
    }
}
