package com.example.buyit;

public class product {
   private String id;
  private  String name;
  private  double price;
  private  String is_favourite;
  private  String rate;
  private  String logo;
  private  String in_cart;

    public product(String id, String name, double price, String rate, String logo) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.rate = rate;
        this.logo = logo;
    }

    public product(String id, String name, double price, String is_favourite, String rate, String logo, String in_cart) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.is_favourite = is_favourite;
        this.rate = rate;
        this.logo = logo;
        this.in_cart = in_cart;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getIs_favourite() {
        return is_favourite;
    }

    public void setIs_favourite(String is_favourite) {
        this.is_favourite = is_favourite;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getIn_cart() {
        return in_cart;
    }

    public void setIn_cart(String in_cart) {
        this.in_cart = in_cart;
    }
}
