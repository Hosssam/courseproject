package com.example.buyit;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class prdocutadapter extends RecyclerView.Adapter<prdocutadapter.viewholder> {
public  ArrayList<product>products;
public String token;
public int function;


    public prdocutadapter(ArrayList<product>products, String token,int function) {
        this.products= new ArrayList<>();
        this.products=products;
        this.token=token;
        this.function=function;
    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.productraw,parent,false);
        return new viewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final viewholder holder, final int position) {
        if(function==0){
            holder.cart.setVisibility(View.GONE);
            holder.favorutie.setVisibility(View.GONE);
            Picasso.get().load(products.get(position).getLogo()).into(holder.image);
            holder.name.setText(products.get(position).getName());
            holder.price.setText("" + products.get(position).getPrice());
        }
        else {
            Picasso.get().load(products.get(position).getLogo()).into(holder.image);
            holder.name.setText(products.get(position).getName());
            holder.price.setText("" + products.get(position).getPrice());

            if (Integer.parseInt(products.get(position).getIs_favourite()) == 1) {
                holder.favorutie.setImageResource(R.drawable.ic_like);
            }
            holder.cart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    JSONObject object = new JSONObject();
                    try {
                        object.put("token", token);
                        object.put("product_id", products.get(position).getId());
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, Url.addtocart(), object, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (Integer.parseInt(response.getString("status")) == 0) {
                                        Toast.makeText(view.getContext(), "sucessfully added,check your cart", Toast.LENGTH_SHORT).show();
                                    } else if (Integer.parseInt(response.getString("status")) == 222    ) {

                                        Toast.makeText(view.getContext(), "already in cart", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(view.getContext(), "something went wrong", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });
                        volleysingletoon volleysingletoon = com.example.buyit.volleysingletoon.getInstance(view.getContext());
                        volleysingletoon.addrequest(request);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
            holder.favorutie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    JSONObject object = new JSONObject();
                    try {
                        object.put("token", token);
                        object.put("product_id", products.get(position).getId());
                        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/add_favourite.php", object, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    Log.d("favourite", "onResponse: " + response.toString());
                                    String status = response.getString("status");
                                    if (Integer.parseInt(status) == 0) {
                                        holder.favorutie.setImageResource(R.drawable.ic_like);

                                    } else if (Integer.parseInt(status) == 220) {
                                        JSONObject object1 = new JSONObject();
                                        object1.put("token", token);
                                        object1.put("product_id", products.get(position).getId());
                                        JsonObjectRequest request1 = new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/delete_favourites.php", object1, new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                try {
                                                    Log.d("delete from favourites", "onResponse: " + response.toString());
                                                    String status = response.getString("status");
                                                    if (Integer.parseInt(status) == 0) {
                                                        holder.favorutie.setImageResource(R.drawable.ic_heart);
                                                    } else if (Integer.parseInt(status) == 505) {
                                                        Toast.makeText(view.getContext(), "error while deleting", Toast.LENGTH_SHORT).show();
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        }, new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                Log.d("error", "onErrorResponse: " + error.toString());

                                            }
                                        });
                                        volleysingletoon volleysingletoon = com.example.buyit.volleysingletoon.getInstance(view.getContext());
                                        volleysingletoon.addrequest(request1);

                                        Toast.makeText(view.getContext(), "removed", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(view.getContext(), "error", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("error ll2sf", "onErrorResponse: " + error.toString());
                            }
                        });
                        volleysingletoon volleysingletoon = com.example.buyit.volleysingletoon.getInstance(view.getContext());
                        volleysingletoon.addrequest(request);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent z = new Intent(view.getContext(),product_detail.class);
                z.putExtra("product_id",products.get(position).getId());
                view.getContext().startActivity(z);
            }
        });
    }



    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class viewholder extends RecyclerView.ViewHolder{
    public ImageView image;
    public TextView name;
    public TextView price;
    public TextView cart;
    public ImageView favorutie;
    public ConstraintLayout layout;
        public viewholder(@NonNull View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.product_image);
            name=itemView.findViewById(R.id.product_title);
            price=itemView.findViewById(R.id.product_price);
            cart=itemView.findViewById(R.id.cart);
            favorutie=itemView.findViewById(R.id.product_favourite);
            layout=itemView.findViewById(R.id.product_row);
        }
    }
}
