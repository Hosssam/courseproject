package com.example.buyit;

public class Response {
    String msg;
    String status;
    Profileclass data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Profileclass getData() {
        return data;
    }

    public void setData(Profileclass data) {
        this.data = data;
    }
}
