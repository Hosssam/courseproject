package com.example.buyit;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class favourite_frag extends Fragment {
public ArrayList<product>products;
public RecyclerView recyclerView;
public String token;

    public favourite_frag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_favourite_frag, container, false);
        if(getArguments()!=null){
        products=new ArrayList<>();
            recyclerView=v.findViewById(R.id.favourite_rec);
            token=getArguments().getString("token");
           requestfavourites(token);
            Log.d("token", "onCreateView: "+getArguments().getString("token"));
        }

        return v;
    }
    public void requestfavourites(final String token){
        JSONObject object= new JSONObject();
        try {
            object.put("token",token);
            object.put("page_number","1");
            object.put("items_per_page","100");

            JsonObjectRequest request= new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/get_favourites.php", object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("favourties", "onResponse: "+response.toString());
                    try {
                        String status=response.getString("status");
                        if(Integer.parseInt(status)==0){
                            JSONArray array=new JSONArray();
                            array=response.getJSONArray("data");
                            for(int i=0;i<array.length();i++){
                                String id=array.getJSONObject(i).getString("id");
                                String name=array.getJSONObject(i).getString("name");
                                Double price=array.getJSONObject(i).getDouble("price");
                                String rate=array.getJSONObject(i).getString("rate");
                                String logo=array.getJSONObject(i).getString("logo");
                                product product=new product(id,name,price,rate,logo);
                                products.add(product);
                            }
                         prdocutadapter prdocutadapter=new prdocutadapter(products,token,0);
                            recyclerView.setAdapter(prdocutadapter);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(getContext());
        volleysingletoon.addrequest(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
