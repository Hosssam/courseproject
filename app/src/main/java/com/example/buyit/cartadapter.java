package com.example.buyit;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class cartadapter extends RecyclerView.Adapter<cartadapter.viewholder> {
public ArrayList<cart>carts;

    public cartadapter(ArrayList<cart> carts) {
        this.carts = carts;
    }

    @NonNull
    @Override
    public viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_row,parent,false);

        return new viewholder(v);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void onBindViewHolder(@NonNull final viewholder holder, final int position) {
        holder.title.setText(carts.get(position).getName());
        holder.number.setText(carts.get(position).getFrequency());
        holder.price.setText(""+carts.get(position).getPrice());
        Picasso.get().load(carts.get(position).getLogo()).into(holder.image);
        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject object= new JSONObject();
                try {
                    object.put("token", view.getContext().getSharedPreferences("file",0).getString("token","0"));
                    object.put("product_id",carts.get(position).getId());
                    object.put("action","decrease");
                    JsonObjectRequest request= new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/change_product_frequency.php", object, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.d("response", "onResponse: "+response.toString());
                                if(Integer.parseInt(response.getString("status"))==0){
                                    holder.number.setText(""+response.getString("frequency"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("error", "onErrorResponse: "+error);
                        }
                    });
                    volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(view.getContext());
                    volleysingletoon.addrequest(request);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject object= new JSONObject();
                try {
                    object.put("token", view.getContext().getSharedPreferences("file",0).getString("token","0"));
                    object.put("product_id",carts.get(position).getId());
                    object.put("action","increase");
                    JsonObjectRequest request= new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/change_product_frequency.php", object, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.d("response", "onResponse: "+response.toString());
                                if(Integer.parseInt(response.getString("status"))==0){
                                    holder.number.setText(""+response.getString("frequency"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("error", "onErrorResponse: "+error);
                        }
                    });
                    volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(view.getContext());
                    volleysingletoon.addrequest(request);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        });
        holder.trash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
             JSONObject object=new JSONObject();
                try {
                    object.put("token", view.getContext().getSharedPreferences("file",0).getString("token","0"));
                    object.put("product_id",carts.get(position).getId());
                    final JsonObjectRequest request=new JsonObjectRequest(Request.Method.POST, "http://training4mobile.net/mobile/delete_from_cart.php", object, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                String status=response.getString("status");
                                if(Integer.parseInt(status)==0){
                                    Log.d("delete", "onResponse:deleted ");
                                    Toast.makeText(view.getContext(), "sucessfully deleted", Toast.LENGTH_SHORT).show();
                                    carts.remove(position);
                                    notifyDataSetChanged();
                                }
                                else if(Integer.parseInt(status)==223){
                                    Toast.makeText(view.getContext(), "already deleted", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    });

                    volleysingletoon volleysingletoon= com.example.buyit.volleysingletoon.getInstance(view.getContext());
                    volleysingletoon.addrequest(request);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent z= new Intent(view.getContext(),product_detail.class);
                z.putExtra("product_id",carts.get(position).getId());
                view.getContext().startActivity(z);
            }
        });

    }



    @Override
    public int getItemCount() {
        return carts.size();
    }

    public static class viewholder extends RecyclerView.ViewHolder{
        ImageView image,plus,minus,trash;
        TextView number,title,price;
        ConstraintLayout layout;
        public viewholder(@NonNull View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.cart_image);
            plus=itemView.findViewById(R.id.cart_plus);
            trash=itemView.findViewById(R.id.trash);
            minus=itemView.findViewById(R.id.cart_minus);
            number=itemView.findViewById(R.id.cart_number);
            title=itemView.findViewById(R.id.cart_name);
            price=itemView.findViewById(R.id.cart_price);
            layout=itemView.findViewById(R.id.linearLayout);
        }
    }
}
